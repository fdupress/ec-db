EC-DB
====

EC-DB aims at extending machine-checked cryptography in EasyCrypt to
distance-bounding protocols (and potentially other types of protocols that rely
on or enforce time constraints).

Its use is illustrated on a simple example protocol, simplified from EMV's RRP.

The "framework"—and in particular the Environment's proof interface—is under
active development, and is likely to undergo drastic changes without notice. It
is meant to explore and illustrate ideas, rather than serve as a stable
interface for further proof development. As much as possible, we will aim to
preserve compatibility of examples included in this repository.

We rely on `match` statements, currently only supported in EasyCrypt's
`preview` branches (for example, `1.0-preview`).