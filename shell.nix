{ ecpath ? easycrypt/default.nix }:

with import <nixpkgs> {};

let ec = (callPackage ecpath { withProvers = true; });
in

pkgs.mkShell {
  buildInputs = [ ec ]
  ++ ec.propagatedBuildInputs
  ++ (with python3Packages; [ pyyaml ]);
}
